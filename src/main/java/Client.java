import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.event.DeviceEventAdapter;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.npdu.ip.IpNetwork;
import com.serotonin.bacnet4j.npdu.ip.IpNetworkBuilder;
import com.serotonin.bacnet4j.service.acknowledgement.ReadPropertyAck;
import com.serotonin.bacnet4j.service.confirmed.ConfirmedRequestService;
import com.serotonin.bacnet4j.service.confirmed.ReadPropertyRequest;
import com.serotonin.bacnet4j.service.confirmed.WritePropertyRequest;
import com.serotonin.bacnet4j.service.unconfirmed.WhoIsRequest;
import com.serotonin.bacnet4j.transport.DefaultTransport;
import com.serotonin.bacnet4j.transport.Transport;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.Double;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;
import com.serotonin.bacnet4j.util.RequestUtils;

import java.util.*;


/* This class is intended to demonstrate basic BACnet functionality using the BACnet4J library.
* INCOMPLETE!
*/
public class Client {

    private static LocalDevice localDevice; // Local workstation/gateway.

    // List of properties to fetch for device info.
    private static final List<PropertyIdentifier> EXTENDED_DEVICE_PROPERTIES = Arrays.asList(
            PropertyIdentifier.objectName,
            PropertyIdentifier.description,
            PropertyIdentifier.objectList,
            PropertyIdentifier.vendorName
    );

    private static final List<PropertyIdentifier> STANDARD_OBJECT_PROPERTIES = Arrays.asList(
            PropertyIdentifier.objectName,
            PropertyIdentifier.description,
            PropertyIdentifier.presentValue,
            PropertyIdentifier.units,
            PropertyIdentifier.outOfService
    );

//    private class ObjectInfo {
//        ObjectIdentifier oid;
//        ReadPropertyAck objectName;
//        ReadPropertyAck description;
//        ReadPropertyAck presentValue;
//        ReadPropertyAck units;
//        ReadPropertyAck outOfService;
//
//        ObjectInfo(ObjectIdentifier oid) {
//            this(oid, null, null, null, null, null);
//        }
//
//        ObjectInfo(ObjectIdentifier oid, ReadPropertyAck objectName, ReadPropertyAck description, ReadPropertyAck presentValue, ReadPropertyAck outOfService, ReadPropertyAck units) {
//            this.oid = oid;
//            this.objectName = objectName;
//            this.description = description;
//            this.presentValue = presentValue;
//            this.outOfService = outOfService;
//            this.units = units;
//        }
//    }

    public static void main(String[] args) {
        // Create new IP network with transport.
        IpNetwork network = new IpNetworkBuilder().withBroadcast("192.168.1.255", 24)
                .build();
        Transport transport = new DefaultTransport(network);

        // Define local device (workstation) with device ID = 35.
        localDevice = new LocalDevice(35, transport);

        // Try to initialize device, add I-Am listener and broadcast global Who-Is.
        try {
            localDevice.initialize();
            localDevice.getEventHandler().addListener(new Listener());
            localDevice.sendGlobalBroadcast(new WhoIsRequest());

            Thread.sleep(15000); // Wait for processes.
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            localDevice.terminate(); // Free BAC0 port.
        }
    }

    static class Listener extends DeviceEventAdapter {
        @Override
        public void iAmReceived(final RemoteDevice d) {
            System.out.println("I-Am message received from " + d);

            // Create new thread to get extended device information.
            new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println("Getting extended device information.");

                        // Retrieve extended device information & print.
                        List<ReadPropertyAck> deviceInfo = getExtendedDeviceInformation(d);

                        for (ReadPropertyAck ack : deviceInfo) {
                            System.out.println("Object: " + ack.getEventObjectIdentifier()
                                    + " | Property: " + ack.getPropertyIdentifier()
                                    + " | Value: " + ack.getValue());
                        }


                        /* Attempt to set the 'SPACE TEMP' present-value (AV#1) to 72.0.
                         * Does not work! */
                        ObjectIdentifier spaceTempOid = new ObjectIdentifier(ObjectType.analogValue, 1);
                        System.out.println(getProperty(d, spaceTempOid, PropertyIdentifier.presentValue).getValue());

                        WritePropertyRequest wpr = new WritePropertyRequest(spaceTempOid, PropertyIdentifier.presentValue,
                                null, new Double(72.0), new UnsignedInteger(1));

                        localDevice.send(d, wpr);
                        System.out.println(getProperty(d, spaceTempOid, PropertyIdentifier.presentValue).getValue());


                        RequestUtils.writeProperty(localDevice, d, spaceTempOid, PropertyIdentifier.presentValue, new Double(72.2));

                        // For-loop for iterating over each object in device. (AI/AO/AV only).
//                        for (ObjectIdentifier oid : oids) {
//                            if ( oid.getObjectType().equals(ObjectType.analogInput) ||
//                                    oid.getObjectType().equals(ObjectType.analogValue) ||
//                                    oid.getObjectType().equals(ObjectType.analogOutput) ) {
//                                // Logic for each analog object.
//                            }
//                        }

                    } catch (BACnetException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    /* Gets extended information of 'device' object in RemoteDevice. */
    private static List<ReadPropertyAck> getExtendedDeviceInformation(RemoteDevice d) throws BACnetException {
        List<ReadPropertyAck> acks = new ArrayList<ReadPropertyAck>();
        ObjectIdentifier oid = d.getObjectIdentifier();

        for (PropertyIdentifier prop : EXTENDED_DEVICE_PROPERTIES) {
            acks.add(getProperty(d, oid, prop));
        }

        return acks;
    }

    /* Axillary helper methods, read-property request. */
    private static ReadPropertyAck getProperty(RemoteDevice d, ObjectType objType, int instanceNumber, PropertyIdentifier property) throws BACnetException {
        ConfirmedRequestService request = new ReadPropertyRequest(new ObjectIdentifier(objType, instanceNumber), property);

        return localDevice.send(d, request).get();
    }

    private static ReadPropertyAck getProperty(RemoteDevice d, ObjectIdentifier oid, PropertyIdentifier property) throws BACnetException {
        ConfirmedRequestService request = new ReadPropertyRequest(oid, property);

        return localDevice.send(d, request).get();
    }

}

//    private void getAllObjectValues (RemoteDevice d, List<ObjectIdentifier> oids) throws BACnetException {
//
//        Map<ObjectIdentifier, ObjectInfo> presentVals = new HashMap<ObjectIdentifier, ObjectInfo>();
//
//        for (ObjectIdentifier oid : oids) {
//            if (oid.getObjectType().equals(ObjectType.analogInput) || oid.getObjectType().equals(ObjectType.analogValue) || oid.getObjectType().equals(ObjectType.analogOutput)) {
//                try {
//                    System.out.println("Trying oid " + oid);
//
//                    ObjectInfo objInfo = new ObjectInfo(oid);
//                    objInfo.objectName =
//
//
//                } catch (BACnetException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        for (ObjectIdentifier oid : presentVals.keySet()) {
//            PropertyValue pv = presentVals.get(oid);
//
//            System.out.println(oid + " | " + pv.getPropertyIdentifier() + " | " + pv.getValue());
//        }
//    }

